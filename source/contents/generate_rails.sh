vagrant@precise:/vagrant/work$ /vagrant/scripts/generate_rails.sh 20131221rbapp
herokuにログインします
Enter your Heroku credentials.
Email: pyar6329@gmail.com
Password (typing will be hidden): 
Authentication successful.
herokuにssh公開鍵を登録します
Found existing public key: /home/vagrant/.ssh/id_rsa.pub
Uploading SSH public key /home/vagrant/.ssh/id_rsa.pub... done
travisコマンドのインストール（アップデート）
Successfully installed travis-1.6.6
1 gem installed
GitHubのアカウントでtravisにloginします
We need your GitHub login to identify you.
This information will not be sent to Travis CI, only to api.github.com.
The password will not be displayed.

Try running with --github-token or --auto if you don't want to enter your password anyways.

Username: pyar6329
Password for pyar6329: ******************
Successfully logged in as pyar6329!
projectディレクトリを作成します
ローカルgitリポジトリを作成します
Initialized empty Git repository in /vagrant/work/20131221rbapp/.git/
Railsアプリを作成します
Invalid application name 20131221rbapp. Please give a name which does not start with numbers.
rake db:migrateを実行します
rake aborted!
No Rakefile found (looking for: rakefile, Rakefile, rakefile.rb, Rakefile.rb)

(See full trace by running task with --trace)
ここまでの内容をcommitします
# On branch master
#
# Initial commit
#
nothing to commit (create/copy files and use "git add" to track)
rspecのためのGemfile設定を追加
Resolving dependencies...
Using rake (0.9.6) 
Using i18n (0.6.5) 
Using minitest (4.3.2) 
Using multi_json (1.8.0) 
Using atomic (1.1.14) 
Using thread_safe (0.1.3) 
Using tzinfo (0.3.37) 
Using activesupport (4.0.0) 
Using builder (3.1.4) 
Using erubis (2.7.0) 
Using rack (1.5.2) 
Using rack-test (0.6.2) 
Using actionpack (4.0.0) 
Using diff-lcs (1.2.5) 
Using thor (0.18.1) 
Using railties (4.0.0) 
Using rspec-core (2.14.7) 
Using rspec-expectations (2.14.4) 
Using rspec-mocks (2.14.4) 
Using rspec-rails (2.14.0) 
Using bundler (1.3.5) 
Your bundle is complete!
Use `bundle show [gemname]` to see where a bundled gem is installed.
rspecの設定を生成
Usage:
  rails new APP_PATH [options]

Options:
  -r, [--ruby=PATH]              # Path to the Ruby binary of your choice
                                 # Default: /home/vagrant/.rbenv/versions/2.0.0-p247/bin/ruby
  -m, [--template=TEMPLATE]      # Path to some application template (can be a filesystem path or URL)
      [--skip-gemfile]           # Don't create a Gemfile
  -B, [--skip-bundle]            # Don't run bundle install
  -G, [--skip-git]               # Skip .gitignore file
      [--skip-keeps]             # Skip source control .keep files
  -O, [--skip-active-record]     # Skip Active Record files
  -S, [--skip-sprockets]         # Skip Sprockets files
  -d, [--database=DATABASE]      # Preconfigure for selected database (options: mysql/oracle/postgresql/sqlite3/frontbase/ibm_db/sqlserver/jdbcmysql/jdbcsqlite3/jdbcpostgresql/jdbc)
                                 # Default: sqlite3
  -j, [--javascript=JAVASCRIPT]  # Preconfigure for selected JavaScript library
                                 # Default: jquery
  -J, [--skip-javascript]        # Skip JavaScript files
      [--dev]                    # Setup the application with Gemfile pointing to your Rails checkout
      [--edge]                   # Setup the application with Gemfile pointing to Rails repository
  -T, [--skip-test-unit]         # Skip Test::Unit files
      [--rc=RC]                  # Path to file containing extra configuration options for rails command
      [--no-rc]                  # Skip loading of extra configuration options from .railsrc file

Runtime options:
  -f, [--force]    # Overwrite files that already exist
  -p, [--pretend]  # Run but do not make any changes
  -q, [--quiet]    # Suppress status output
  -s, [--skip]     # Skip files that already exist

Rails options:
  -h, [--help]     # Show this help message and quit
  -v, [--version]  # Show Rails version number and quit

Description:
    The 'rails new' command creates a new Rails application with a default
    directory structure and configuration at the path you specify.

    You can specify extra command-line arguments to be used every time
    'rails new' runs in the .railsrc configuration file in your home directory.

    Note that the arguments specified in the .railsrc file don't affect the
    defaults values shown above in this help message.

Example:
    rails new ~/Code/Ruby/weblog

    This generates a skeletal Rails installation in ~/Code/Ruby/weblog.
    See the README in the newly created application to get going.
ここまでの内容をcommitします
[master (root-commit) 0e9e2fc] Run rails generate rspec:install
 2 files changed, 52 insertions(+)
 create mode 100644 Gemfile
 create mode 100644 Gemfile.lock
Gemfileにrubyのバージョンを設定します
slite3をdevelopment, testグループのみにします
pgをproductionグループに追加します
bundle installしてGemfile.lockを更新します
Resolving dependencies...
Using rake (0.9.6) 
Using i18n (0.6.5) 
Using minitest (4.3.2) 
Using multi_json (1.8.0) 
Using atomic (1.1.14) 
Using thread_safe (0.1.3) 
Using tzinfo (0.3.37) 
Using activesupport (4.0.0) 
Using builder (3.1.4) 
Using erubis (2.7.0) 
Using rack (1.5.2) 
Using rack-test (0.6.2) 
Using actionpack (4.0.0) 
Using diff-lcs (1.2.5) 
Using thor (0.18.1) 
Using railties (4.0.0) 
Using rspec-core (2.14.7) 
Using rspec-expectations (2.14.4) 
Using rspec-mocks (2.14.4) 
Using rspec-rails (2.14.0) 
Using bundler (1.3.5) 
Your bundle is complete!
Gems in the group production were not installed.
Use `bundle show [gemname]` to see where a bundled gem is installed.
ここまでの内容をcommitします
[master cde0f46] Change database settings in Gemfile
 3 files changed, 13 insertions(+)
 create mode 100644 .bundle/config
welcomeコントローラを生成します
Usage:
  rails new APP_PATH [options]

Options:
  -r, [--ruby=PATH]              # Path to the Ruby binary of your choice
                                 # Default: /home/vagrant/.rbenv/versions/2.0.0-p247/bin/ruby
  -m, [--template=TEMPLATE]      # Path to some application template (can be a filesystem path or URL)
      [--skip-gemfile]           # Don't create a Gemfile
  -B, [--skip-bundle]            # Don't run bundle install
  -G, [--skip-git]               # Skip .gitignore file
      [--skip-keeps]             # Skip source control .keep files
  -O, [--skip-active-record]     # Skip Active Record files
  -S, [--skip-sprockets]         # Skip Sprockets files
  -d, [--database=DATABASE]      # Preconfigure for selected database (options: mysql/oracle/postgresql/sqlite3/frontbase/ibm_db/sqlserver/jdbcmysql/jdbcsqlite3/jdbcpostgresql/jdbc)
                                 # Default: sqlite3
  -j, [--javascript=JAVASCRIPT]  # Preconfigure for selected JavaScript library
                                 # Default: jquery
  -J, [--skip-javascript]        # Skip JavaScript files
      [--dev]                    # Setup the application with Gemfile pointing to your Rails checkout
      [--edge]                   # Setup the application with Gemfile pointing to Rails repository
  -T, [--skip-test-unit]         # Skip Test::Unit files
      [--rc=RC]                  # Path to file containing extra configuration options for rails command
      [--no-rc]                  # Skip loading of extra configuration options from .railsrc file

Runtime options:
  -f, [--force]    # Overwrite files that already exist
  -p, [--pretend]  # Run but do not make any changes
  -q, [--quiet]    # Suppress status output
  -s, [--skip]     # Skip files that already exist

Rails options:
  -h, [--help]     # Show this help message and quit
  -v, [--version]  # Show Rails version number and quit

Description:
    The 'rails new' command creates a new Rails application with a default
    directory structure and configuration at the path you specify.

    You can specify extra command-line arguments to be used every time
    'rails new' runs in the .railsrc configuration file in your home directory.

    Note that the arguments specified in the .railsrc file don't affect the
    defaults values shown above in this help message.

Example:
    rails new ~/Code/Ruby/weblog

    This generates a skeletal Rails installation in ~/Code/Ruby/weblog.
    See the README in the newly created application to get going.
welcomeコントローラをroutesのrootに設定します
sed: config/routes.rb を読み込めません: そのようなファイルやディレクトリはありません
ここまでの内容をcommitします
# On branch master
nothing to commit (working directory clean)
GetHubにリポジトリを作成します
Updating origin
created repository: pyar6329/20131221rbapp
upstreamを設定してGitHubにpushします
Counting objects: 10, done.
Compressing objects: 100% (8/8), done.
Writing objects: 100% (10/10), 1.23 KiB, done.
Total 10 (delta 2), reused 0 (delta 0)
To git@github.com:pyar6329/20131221rbapp.git
 * [new branch]      master -> master
Branch master set up to track remote branch master from origin.
herokuでアプリを作ります
Creating secret-stream-5171... done, stack is cedar
http://secret-stream-5171.herokuapp.com/ | git@heroku.com:secret-stream-5171.git
Git remote heroku added
herokuにpostgresqlアドオンを追加します
Adding heroku-postgresql:dev on secret-stream-5171... done, v3 (free)
Attached as HEROKU_POSTGRESQL_OLIVE_URL
Database has been created and is available
 ! This database is empty. If upgrading, you can transfer
 ! data from another database with pgbackups:restore.
Use `heroku addons:docs heroku-postgresql` to view documentation.
herokuにアプリをdeployします
Initializing repository, done.
Counting objects: 10, done.
Compressing objects: 100% (8/8), done.
Writing objects: 100% (10/10), 1.23 KiB, done.
Total 10 (delta 2), reused 0 (delta 0)

-----> Ruby app detected
-----> Compiling Ruby/Rails
-----> Using Ruby version: ruby-2.0.0
-----> Installing dependencies using Bundler version 1.3.2
       New app detected loading default bundler cache
       Running: bundle install --without development:test --path vendor/bundle --binstubs vendor/bundle/bin --deployment
       Your Gemfile has no remote sources. If you need gems that are not already on
       your machine, add a line like this to your Gemfile:
       source 'https://rubygems.org'
       Could not find pg-0.17.1 in any of the sources
       Bundler Output: Your Gemfile has no remote sources. If you need gems that are not already on
       your machine, add a line like this to your Gemfile:
       source 'https://rubygems.org'
       Could not find pg-0.17.1 in any of the sources
 !
 !     Failed to install gems via Bundler.
 !

 !     Push rejected, failed to compile Ruby app

To git@heroku.com:secret-stream-5171.git
 ! [remote rejected] master -> master (pre-receive hook declined)
error: failed to push some refs to 'git@heroku.com:secret-stream-5171.git'
travisを初期化します
repository not known to Travis CI (or no access?)
triggering sync: . done
Main programming language used: |Ruby| .travis.yml file created!
pyar6329/20131221rbapp: enabled :)

.travis.ymlを上書き（travis initだと，使用しないversionのrubyも設定される）
ここまでの内容をcommitします
[master f135e91] Create .travis.yml
 1 file changed, 3 insertions(+)
 create mode 100644 .travis.yml
.travis.ymlファイルにherokuのための情報を追加します
Deploy only from pyar6329/20131221rbapp? |yes| Encrypt API key? |yes| 
.travis.ymlファイルにdb:migrateのための情報を追加します
ここまでの内容をcommitします
[master 00f6019] Add settings for heroku into .travis.yml
 1 file changed, 8 insertions(+)
GitHubにpushします
Warning: Permanently added the RSA host key for IP address '192.30.252.131' to the list of known hosts.
Counting objects: 7, done.
Compressing objects: 100% (5/5), done.
Writing objects: 100% (6/6), 874 bytes, done.
Total 6 (delta 1), reused 0 (delta 0)
To git@github.com:pyar6329/20131221rbapp.git
   cde0f46..00f6019  master -> master
アプリを20131221rbappディレクトリに作成しました
cd 20131221rbapp を実行してください
しばらくするとtravisでbuildがはじまります
heroku上のアプリは下記Web URLからアクセスできます
=== secret-stream-5171
Addons:        heroku-postgresql:dev
Git URL:       git@heroku.com:secret-stream-5171.git
Owner Email:   pyar6329@gmail.com
Region:        us
Stack:         cedar
Tier:          Legacy
Web URL:       http://secret-stream-5171.herokuapp.com/